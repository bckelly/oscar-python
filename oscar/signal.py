
# A Signal is a type of measurement. It has physical and digital attributes, and all individual samples of the signal
# Are stored with the signal. TODO: At least for now. Eventually they will be stored in the database.
class Signal:
    def __init__(self):
        self.label = ""
        self.transducer = ""
        self.physical_dimension = ""
        self.physical_minimum = ""
        self.physical_maximum = ""
        self.digital_minimum = ""
        self.digital_maximum = ""
        self.prefiltering = ""
        self.samples_per_record = ""
        self.reserved = ""
        self.samples_array = []
