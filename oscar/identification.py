import os

# This file handles the Identification.tgt file

# Keys in the Identification.tgt file, called out as constants
# The file typically has this kind of format:
#
# #IMF 0001
#
# #VIR 0065
#
# #RIR 0064
#
# #PVR 0065
#
# #PVD 001A
# ... etc
IMF = "IMF"
VIR = "VIR"
RIR = "RIR"
PVR = "PVR"
PVD = "PVD"
CID = "CID"
RID = "RID"
VID = "VID"
SRN = "SRN"
SID = "SID"
PNA = "PNA"
PCD = "PCD"
PCB = "PCB"
MID = "MID"
FGT = "FGT"
BID = "BID"
IDENTIFICATION_FILE_NAME = "Identification.tgt"


class Identification:
    def __init__(self):
        self.imf = ""
        self.vir = ""
        self.rir = ""
        self.pvr = ""
        self.pvd = ""
        self.cid = ""
        self.rid = ""
        self.vid = ""  # Found in the STR.edf, unknown meaning
        self.srn = ""  # Serial Number
        self.sid = ""
        self.pna = ""  # Product Name
        self.pcd = ""  # Product Code
        self.pcb = ""  # PCB Number (Printed Circuit Board)
        self.mid = ""  # Found in the STR.edf, unknown meaning
        self.fgt = ""
        self.bid = ""


# Given the raw product name from the file, strip it down and give it a pretty
# display name.
def parse_product_name(name_string):
    # We expect the value to be one of:
    # AirSense 11
    # AirSense 10
    # AirCurve 11
    # AirCurve 10 -- I don 't think this exists?
    # S9

    name_string = name_string.replace("_", " ")
    name_string = name_string.replace("(", "")
    name_string = name_string.replace(")", "")

    # If it is not AirCurve or AirSense, it is a Series 9
    if "AirSense" not in name_string and "AirCurve" not in name_string:
        # In OSCAR, we assumed it was an S9, so prepend with that
        name_string = "S9 " + name_string

    return name_string


# Read the pieces of the Identification.tgt file and generate an object to be
# used in the profile later.
def parse_identification_file(sleep_directory):
    identification = Identification()
    filename = sleep_directory + "/" + IDENTIFICATION_FILE_NAME
    exists = os.path.exists(filename)
    if exists:
        with open(filename) as f:
            lines = f.readlines()

            for line in lines:
                line = line.replace("#", "")
                parts = line.split(" ")
                key = parts[0]
                value = parts[1]

                # TODO: These should be referencing the constants
                match key:
                    case "IMF":
                        identification.imf = value
                    case "VIR":
                        identification.vir = value
                    case "RIR":
                        identification.rir = value
                    case "PVR":
                        identification.pvr = value
                    case "PVD":
                        identification.pvd = value
                    case "CID":
                        identification.cid = value
                    case "RID":
                        identification.rid = value
                    case "VID":
                        identification.vid = value
                    case "SRN":
                        identification.srn = value
                    case "SID":
                        identification.sid = value
                    case "PNA":
                        identification.pna = parse_product_name(value)
                    case "PCD":
                        identification.pcd = value
                    case "PCB":
                        identification.pcb = value
                    case "MID":
                        identification.mid = value
                    case "FGT":
                        identification.fgt = value
                    case "BID":
                        identification.bid = value
                    case _:
                        print(
                            "Unknown column type found in identification file:"
                            " {key}, value: {value}, full line: {line}".format(
                                key=key, value=value, line=line))
        return identification
    else:
        print("File doesn't exist: " + filename)
