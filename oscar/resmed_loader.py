import glob

from django.conf import settings
from oscar import edf_parser

DATALOG_FOLDER = "DATALOG"
PRIMARY_STR_EDF_FILE = "STR.edf"
SECONDARY_STR_EDF_FILE = "STR.edf.gz"


# TODO: I had a difficult thought. If this is going to be a web server,
#  transferring an SD card full of data into it Is going to be very time
#  consuming. 1+GB over HTTP is dog slow, we may need to rethink that.


# Find all Files with postfix (default: edf) in the application data directory
def list_files_in_directory(file_type="edf"):
    directory = settings.OSCAR_DATA_LOAD_DIRECTORY
    filename = directory + "/**/*." + file_type
    return glob.glob(filename, recursive=True)


# Given a set of EDF files, parse them, save their content and print something.
def load_edf_files(files):
    for file in files:
        content = edf_parser.parse_edf_file(file)
        if content != {}:
            # Valid file!
            print("file: " + file + " content: " + content["header"]["labels"][0])


def load_resmed_data():
    files = list_files_in_directory()
    load_edf_files(files)
