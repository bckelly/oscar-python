from django.http import HttpResponse

from oscar import resmed_loader


def index(request):
    return HttpResponse("Hello, world. You're at the oscar index.")


def load_resmed_data(request):
    resmed_loader.load_resmed_data()
    return HttpResponse("Loading EDF data!")
