import codecs
from datetime import datetime
import os
from oscar.models import Signal, Sample, File, Annotation

# File documentation:
#   This file extracts any and all useful information out of an
#   EDF (European Data Format) file. The definition of an EDF file comes from
#   here: https://www.edfplus.info/specs/edf.html

# Types of EDF files
EDF_EVE = "EVE"  # Event annotations, Central Apnea, Apnea, Hypopnea, etc
EDF_BRP = "BRP"  # High resolution graph data
EDF_PLD = "PLD"  # Low resolution graph data
EDF_SAD = "SAD"  # Pulse oximetry data
EDF_CSL = "CSL"  # Cheynes-Stokes Respiration annotations
EDF_AEV = "AEV"  # TODO: Existed in OSCAR but never seemed to be used.
EDF_CRC = "CRC"  # Checksum file.
# Crc16 is mentioned in the annotations, maybe related?

# I'm assuming all the files will be in utf-8 format ish.
ENCODING = 'utf-8'

ANNOTATION_SEPARATION = b'\x14'
ANNOTATION_DURATION = b'\x15'
ANNOTATION_END = b'\x00'


# The beginning of the header is very critical.
# It describes what the rest of the header and the data section
# will be structured as.
class EdfHeaderStart:
    def __init__(self):
        self.version = ""
        self.patient_id = ""
        self.recording_id = ""
        self.start_date = ""
        self.start_time = ""
        self.bytes_in_record = 0
        self.reserved = ""
        self.number_of_records = 0
        self.duration_in_seconds = 0.0
        self.number_of_signals = 0


# Each individual Annotation sample has 4 components in order:
#   Offset: The seconds since the start time when the annotation occurs
#   Duration: Time in seconds of how long the annotation occurs
#   Unknown: Extra column that is not described in OSCAR but exists in the data
#   Label: Name of the annotation. Ex: "Central Apnea" or "Arousal"
class EdfAnnotation:
    def __init__(self):
        self.offset = -1
        self.duration = -1.0
        self.unknown = -1
        self.label = ""


# The first 256 bytes of the header describe what the rest of the file is going
# to look like. It is separated into a more logical chunk here.
def parse_edf_header_start(f):
    edf_header = EdfHeaderStart()
    edf_header.version = str(f.read(8), ENCODING).strip()
    edf_header.patient_id = str(f.read(80), ENCODING).strip()
    edf_header.recording_id = str(f.read(80), ENCODING).strip()
    edf_header.start_date = str(f.read(8), ENCODING).strip()
    edf_header.start_time = str(f.read(8), ENCODING).strip()
    edf_header.bytes_in_record = int(str(f.read(8), ENCODING).strip())
    edf_header.reserved = str(f.read(44), ENCODING).strip()
    edf_header.number_of_records = int(str(f.read(8), ENCODING).strip())
    edf_header.duration_of_data_record_in_seconds = float(
        str(f.read(8), ENCODING).strip())
    edf_header.number_of_signals = int(str(f.read(4), ENCODING).strip())
    return edf_header


# For each type in the second section of the header, extract the specified
# number of bytes and return an array of the fields that it finds.
def extract_fields(f, number_of_signals, length):
    fields = []
    for index in range(number_of_signals):
        field = str(f.read(length), ENCODING).strip()
        fields.append(field)
    return fields


# The header of the EDF file describes the contents of the data, so take apart
# the header first, so we know how to parse the data structure afterwards.
def parse_edf_header(f):
    start = parse_edf_header_start(f)
    number_of_signals = start.number_of_signals

    header = {
        "start": start,
        "labels": extract_fields(f, number_of_signals, 16),
        "transducers": extract_fields(f, number_of_signals, 80),
        "physical_dimensions": extract_fields(f, number_of_signals, 8),
        "physical_minimums": extract_fields(f, number_of_signals, 8),
        "physical_maximums": extract_fields(f, number_of_signals, 8),
        "digital_minimums": extract_fields(f, number_of_signals, 8),
        "digital_maximums": extract_fields(f, number_of_signals, 8),
        "prefilterings": extract_fields(f, number_of_signals, 80),
        "samples_per_records": extract_fields(f, number_of_signals, 8),
        "reserveds": extract_fields(f, number_of_signals, 32),
    }

    return header


# Assumes the date is dd.mm.yy and the time is hh.mm.ss.ms
def create_date_time_from_strings(date, time):
    together = date + '.' + time
    parsed_date = datetime.strptime(together, '%d.%m.%y.%H.%M.%S')
    return parsed_date


# Generate an array of Signal objects out of the named/indexed arrays. This is
# easier to traverse than an integer-indexed array of integer-indexed values.
def generate_signal_types(header, file):
    number_of_signals = header["start"].number_of_signals
    signals = []
    for signal_index in range(number_of_signals):
        signal = Signal(
            inserted_date=datetime.utcnow(),
            recorded_date=create_date_time_from_strings(
                header["start"].start_date,
                header["start"].start_time
            ),
            label=header["labels"][signal_index],
            transducer=header["transducers"][signal_index],
            physical_dimension=header["physical_dimensions"][signal_index],
            physical_minimum=header["physical_minimums"][signal_index],
            physical_maximum=header["physical_maximums"][signal_index],
            digital_minimum=header["digital_minimums"][signal_index],
            digital_maximum=header["digital_maximums"][signal_index],
            prefiltering=header["prefilterings"][signal_index],
            samples_per_record=header["samples_per_records"][signal_index],
            reserved=header["reserveds"][signal_index],
            file=file
        )
        signal.save()
        signals.append(signal)
    return signals


# If we run into a separation style character, whether it's a duration,
# separation or end character, skip to the end of the block of characters until
# we find something that isn't a separation character.
def skip_to_next_character(f):
    character = f.read(1)
    while character == ANNOTATION_DURATION:
        character = f.read(1)
    while character == ANNOTATION_SEPARATION:
        character = f.read(1)
    while character == ANNOTATION_END:
        character = f.read(1)
    return character


# The first part of an annotation is the offset.
# Extract the offset from the annotation.
def parse_offset(f):
    sign = str(skip_to_next_character(f), ENCODING)
    sign_value = -1
    if sign == "+":
        sign_value = True
    elif sign == "-":
        sign_value = False
    else:
        print("Error! Sign value was not a valid value!")

    offset = ""
    character = f.read(1)
    while character != ANNOTATION_SEPARATION:
        offset += str(character, ENCODING)
        character = f.read(1)

    offset_int = int(offset)
    if not sign_value:
        offset_int = -offset_int

    return offset_int


# The second part of an annotation is the duration.
# Extract that from the annotation.
def parse_duration(f):
    sign = str(skip_to_next_character(f), ENCODING)
    sign_value = -1
    if sign == "+":
        sign_value = True
    elif sign == "-":
        sign_value = False
    else:
        print("Error! Sign value was not a valid value!")

    duration = ""
    character = f.read(1)
    while character != ANNOTATION_DURATION:
        duration += str(character, ENCODING)
        character = f.read(1)

    duration_int = float(duration)
    if not sign_value:
        duration_int = -duration_int

    return duration_int


# The fourth part of the annotation is the label.
# Extract that from the annotation.
def parse_annotation_label(f):
    label = ""
    character = skip_to_next_character(f)
    while character != ANNOTATION_SEPARATION:
        label += str(character, ENCODING)
        character = f.read(1)

    return label


# After the annotation label, scroll to the end of the annotation. Take the
# start of the annotation and calculate where the end of the annotation should
# be. Set the file index to that position.
def go_to_last_end_byte(f, start_position, annotation_length):
    current_position = f.tell()
    end_position = start_position + (annotation_length * 2)
    steps_to_end = end_position - current_position
    f.seek(steps_to_end, 1)  # Skip forward to the end of the annotation block
    return f.tell()  # The final index we scrolled to. Useful for debugging.


# Extract an Annotation object from the file
def parse_annotation(f, signal):
    annotation_length = int(signal.samples_per_record)
    start_position = f.tell()
    offset = parse_offset(f)
    duration = parse_duration(f)
    # Runs basically the same, unknown what the purpose is
    unknown_field = int(parse_annotation_label(f))
    label = parse_annotation_label(f)

    # This could all be in the constructor, but I feel that is harder to debug
    annotation = Annotation(
        offset=offset,
        duration=duration,
        unknown=unknown_field,
        label=label,
        signal=signal
    )
    annotation.save()

    # Rather than check each of the following characters and find the last one
    #  That isn't an end character type, we know we're at the end of the
    #  Annotation, so calculate and set the pointer to the last reasonable
    #  Character's index.
    go_to_last_end_byte(f, start_position, annotation_length)
    return annotation


# After the definitions of the header and signals are extracted, parse the data
# points out of the EDF file.
def parse_edf_data(f, header, signals):
    records = header["start"].number_of_records
    for record_index in range(records):
        for signal in signals:
            if "Annotations" in signal.label:
                parse_annotation(f, signal)
            else:
                # TODO: is that it? Was it actually that easy? This is way
                #   Shorter/Simpler than the original OSCAR code. Watch this
                #   very carefully, I don't believe it.
                value = f.read(2)
                sample_value = int(codecs.encode(value, 'hex'), 16)
                sample = Sample(
                    signal=signal,
                    value=sample_value
                )
                sample.save()


# Extract all information out of the EDF file into a program-usable format.
def parse_edf_file(filename):
    file = File(
        file_path=filename,
        parsed_date=datetime.utcnow()
    )
    file.save()

    with open(filename, "rb") as f:
        file_size = os.fstat(f.fileno()).st_size
        if file_size < 256:
            return {}  # File size is too small!
        header = parse_edf_header(f)
        signals = generate_signal_types(header, file)
        parse_edf_data(f, header, signals)

    return {"header": header, "signals": signals}
