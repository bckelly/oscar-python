import platform

Windows = "Windows"
Linux = "Linux"
OSx = "OSx"
Unknown = "Unknown?"


def find_os_type():
    os_type = platform.platform().split("-")[0]
    if os_type == Windows:
        return Windows
    elif os_type == Linux:
        return Linux
    elif os_type == OSx:
        return OSx
    else:
        return Unknown
