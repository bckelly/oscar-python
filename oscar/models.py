from django.db import models


class File(models.Model):
    file_path = models.CharField(max_length=300)
    parsed_date = models.DateTimeField('Date parsed')


class Signal(models.Model):
    file = models.ForeignKey(File, on_delete=models.CASCADE)
    inserted_date = models.DateTimeField('Date inserted')
    recorded_date = models.DateTimeField('Date recorded')
    label = models.CharField(max_length=200)
    transducer = models.CharField(max_length=200)
    physical_dimension = models.CharField(max_length=200)
    physical_minimum = models.FloatField()
    physical_maximum = models.FloatField()
    digital_minimum = models.IntegerField()
    digital_maximum = models.IntegerField()
    prefiltering = models.CharField(max_length=200)
    samples_per_record = models.CharField(max_length=200)
    reserved = models.CharField(max_length=200)


class Sample(models.Model):
    signal = models.ForeignKey(Signal, on_delete=models.CASCADE)
    value = models.IntegerField()
    #  TODO: doesn't each sample have a timestamp associated with it?
    #    Maybe just an offset?


class Annotation(models.Model):
    signal = models.ForeignKey(Signal, on_delete=models.CASCADE)
    offset = models.IntegerField()
    duration = models.FloatField()
    unknown = models.FloatField()
    label = models.CharField(max_length=200)
