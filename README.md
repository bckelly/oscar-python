# OSCAR
OSCAR is the <b>O</b>pen <b>S</b>ource <b>C</b>PAP <b>A</b>nalysis <b>R</b>eporter.
It will graph the data recorded by your CPAP machine in a human-readable format.

This OSCAR version is a rewrite of the original OSCAR repository [here](https://gitlab.com/pholy/OSCAR-code/-/tree/master/) in order to further the development of an outstanding idea.
The original code proved difficult to maintain and expand, so a rewrite and simplification
was necessary.

# Features
- [X] Extracts minute-by-minute measurements from data files on SD cards from the CPAP machine.
- [X] Parses EDF files from ResMed machines
- [ ] Parses CSV files
- [ ] Parses TSV files
- [ ] Original machine support
  - [ ] CMS50
  - [ ] CMS50F37 (?)
  - [ ] Dreem
  - [ ] Intellipap
  - [ ] MD300W1
  - [ ] M-Series
  - [ ] Prisma
  - [ ] PRS1
  - [ ] Sleep Style
  - [ ] Somnopose
  - [ ] Weinmann
  - [ ] Zeo
  - [X] ResMed
  - [ ] Viatom
  - [ ] Migration of original OSCAR to new data structure
- [ ] UI from OSCAR is translated to web-based UI
  - [ ] About Page
  - [ ] Home Page
  - [ ] Profile setup pages
  - [ ] Import Progress bar
  - [ ] Export page
  - [ ] Profile selector
  - [ ] Update page
  - [ ] Welcome page
- [ ] Translation support
  - [ ] Understand and implement translation framework in Python
  - [ ] Auto-import from XML files from QT translation framework
- [X] Data is saved to SQLite Database
  - [X] Definition of SQLite Database
  - [X] Auto-creation of database and structure
- [ ] Profiles similar to OSCAR are supported
  - [ ] Preferences
- [ ] Automatic detection of SD card
- [ ] Graphical display of data
- [ ] Code coverage
- [ ] Docker build
- [X] Dynamic configuration of database
- [X] Multiple types of databases supported
  - [X] MySQL/MariaDB
  - [X] PostGres
  - [X] SQLite
- [ ] Gitlab Structure
  - [ ] Wiki describing behavior of application and detailed information on the code
  - [ ] CI/CD pipeline
  - [ ] Auto-deploy of artifacts to Gitlab
  - [ ] Set up issue tracker
  - [ ] pre-commit linter checks
  - [ ] Test framework
  - [ ] Merge request guidelines

# Getting started
This project is a work in progress! It is built using [PyCharm](https://www.jetbrains.com/pycharm/) but is constructed
to be easy to work with using your IDE of choice. It should work just fine using Python 3, Pip, and Pipenv.
To download all dependencies, ensure python 3, pip and pipenv are installed, then run `pipenv shell` and run `pip install` (*This may be incorrect or incomplete, please help with instructions!*)
To start the server, run `python manage.py runserver 8000`
Note: Django is a work in progress and may not be fully featured until the back end logic is written.
Initialize the database: `python manage.py migrate`

# Contributing
Help is always welcome! If you have an idea for something new, or know how to fix something,
please fork the project, create a new branch, and create a merge request. Your changes will
be reviewed, discussed, modified and eventually merged into the project! Smaller changes
are always preferred simply because they are easy to review and potential breaks are easily
discovered prior to being released.

# Bugs
They are a fact of life, code is never perfect. If you find one, please categorize it to the best of your abilities and file it using Gitlab's [Issue Tracker](https://gitlab.com/bckelly/oscar-python/-/issues)

# Similar projects
- [OSCAR](https://gitlab.com/pholy/OSCAR-code/-/tree/master/) - Python code translated and based directly on the OSCAR project.
- [SleepyHead](https://gitlab.com/sleepyhead/sleepyhead-code) - Original, underlying code for much of OSCAR is based directly on SleepyHead
